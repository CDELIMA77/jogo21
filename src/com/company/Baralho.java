package com.company;

import java.util.ArrayList;
import java.util.HashMap;

public class Baralho {
    public HashMap<String, Integer> inicializaBaralho() {
        HashMap<String, Integer> baralho = new HashMap<>();

        baralho.put("OuroAs", 1);
        baralho.put("Ouro2", 2);
        baralho.put("Ouro3", 3);
        baralho.put("Ouro4", 4);
        baralho.put("Ouro5", 5);
        baralho.put("Ouro6", 6);
        baralho.put("Ouro7", 7);
        baralho.put("Ouro8", 8);
        baralho.put("Ouro9", 9);
        baralho.put("Ouro10", 10);
        baralho.put("OuroJ", 10);
        baralho.put("OuroQ", 10);
        baralho.put("OuroK", 10);

        baralho.put("PausAs", 1);
        baralho.put("Paus2", 2);
        baralho.put("Paus3", 3);
        baralho.put("Paus4", 4);
        baralho.put("Paus5", 5);
        baralho.put("Paus6", 6);
        baralho.put("Paus7", 7);
        baralho.put("Paus8", 8);
        baralho.put("Paus9", 9);
        baralho.put("Paus10", 10);
        baralho.put("PausJ", 10);
        baralho.put("PausQ", 10);
        baralho.put("PausK", 10);

        baralho.put("EspadaAs", 1);
        baralho.put("Espada2", 2);
        baralho.put("Espada3", 3);
        baralho.put("Espada4", 4);
        baralho.put("Espada5", 5);
        baralho.put("Espada6", 6);
        baralho.put("Espada7", 7);
        baralho.put("Espada8", 8);
        baralho.put("Espada9", 9);
        baralho.put("Espada10", 10);
        baralho.put("EspadaJ", 10);
        baralho.put("EspadaQ", 10);
        baralho.put("EspadaK", 10);


        return baralho;
    }

}