package com.company;

public class Impressora {
    public static void inicioJogo() {
        System.out.println("Início do jogo. Carta Sorteada:");
    }

    public static void exibirMensagemContinua() {
            System.out.println("Comprar carta: S/N :");
    }

    public static void exibirCartas(Baralho baralho) {
        System.out.println("Cartas sorteadas :" + baralho);
    }

    public static void imprimirHistorico(int valor) {
         System.out.println("Sua melhor pontuação foi: " + valor);
    }
}

