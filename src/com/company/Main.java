package com.company;

// Será criado um sistema para jogar 21 em um terminal. A versão inicial do jogo será para um único jogador.
// A ideia é que o jogador possa jogar diversas vezes etenha um histórico da sua melhor rodada.
//      Em cada rodada, o jogador irá definir se deseja continuar comprando cartas ou se ele deseja desistir
//      O sorteio deve ser feito em cima de um baralho que possua a quantidade de cartas convencional ( sem coringa )
//      Dessa forma, uma carta já sorteada não pode ser repetida
//      Caso o jogador ultrapasse 21 pontos ou caso ele desista, a rodada é finalizada e o histórico e atualizado
//      O histórico consiste unicamente da melhor pontuação que o jogador obteve, sendo desconsiderada rodada acima de 21 pontos
//      Na versão inicial do jogo, o histórico e gravado em memoria e e perdido qdo encerra o jogo.

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        Sorteador sorteador = new Sorteador();
        HashMap<String, Integer> novoBaralho =  new HashMap<>();
        ArrayList<String> baralhoIndices = new ArrayList<>();
        ArrayList<String> listaSorteados = new ArrayList<>();

        Baralho baralho = new Baralho();


        novoBaralho = baralho.inicializaBaralho();

        for(String carta : novoBaralho.keySet()){
            baralhoIndices.add(carta);
        }

        // System.out.println(listaSorteados);

        int totalPontos = 0;
        int historico = 0;

        Impressora.inicioJogo();

        //Adiciona carta na lista e imprime
        listaSorteados.add(sorteador.comprarCarta(baralhoIndices));
        //Impressora.exibirCartas(listaSorteados);

        listaSorteados = sorteador.continuaComprando(listaSorteados);

//        if (totalPontos > historico) {
//            historico = totalPontos;
//        }
        Impressora.imprimirHistorico(historico);
    }
}
